# Copyright 2019 Sorama B.V.

set (LIB_NAME sorama-pywavelets)
set(LIB_HEADERS 
	${CMAKE_CURRENT_LIST_DIR}/common.h
	${CMAKE_CURRENT_LIST_DIR}/convolution.h
	${CMAKE_CURRENT_LIST_DIR}/convolution.template.h
	${CMAKE_CURRENT_LIST_DIR}/templating.h
	${CMAKE_CURRENT_LIST_DIR}/wavelets.h
	${CMAKE_CURRENT_LIST_DIR}/wt.h
	${CMAKE_CURRENT_LIST_DIR}/wt.template.h)

set(LIB_SOURCES
	# NOTE: There are more .c files in this folder. However, these are internally
	# used files and only work when they are included via other .c files (blame original coder).
	# If you add them in cmake, cmake will try to compile them on their own, which fails.
	${CMAKE_CURRENT_LIST_DIR}/common.c
	${CMAKE_CURRENT_LIST_DIR}/convolution.c
	${CMAKE_CURRENT_LIST_DIR}/cwt.c
	${CMAKE_CURRENT_LIST_DIR}/wavelets.c
	${CMAKE_CURRENT_LIST_DIR}/wt.c)
    
# Check if we need to generate static or shared (order of if/elseif is intentional)
if(SORAMA_GLOBAL_LINKAGE STREQUAL "STATIC")
    set(SORAMA_STATIC "TRUE")
elseif(SORAMA_GLOBAL_LINKAGE STREQUAL "SHARED")
    set(SORAMA_SHARED "TRUE")
else()  
    #default to STATIC
    set(SORAMA_STATIC "TRUE")
endif()

# Add LIB_HEADERS to LIB_SOURCES
if(LIB_HEADERS)
    set(LIB_SOURCES ${LIB_SOURCES} ${LIB_HEADERS})
endif()

# Determine paths
include(GNUInstallDirs)
set(SORAMA_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}")

# Get subdirectory for header files
file(RELATIVE_PATH SORAMA_HEADERS_PATH ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_LIST_DIR})
set(SORAMA_INSTALL_INCLUDEDIR "${CMAKE_INSTALL_INCLUDEDIR}/${SORAMA_HEADERS_PATH}")

if(SORAMA_SHARED)
    # Somehow findcuda fails to get the include directories for a target in older scripts
    add_library(${LIB_NAME} SHARED ${LIB_SOURCES})
elseif(SORAMA_STATIC)
	add_library(${LIB_NAME} STATIC ${LIB_SOURCES})
    # Enable -fPIC so that static libs can be linked to in a target
    # that is a dynamic library (mainly required in classification repo)
    set_property(TARGET ${LIB_NAME} PROPERTY POSITION_INDEPENDENT_CODE ON)
else()
	message(FATAL_ERROR "No linkage set for PyWavelets")
endif()
	
target_include_directories(${LIB_NAME}
PUBLIC
	$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
	$<INSTALL_INTERFACE:${SORAMA_INSTALL_INCLUDEDIR}>)

# Check if the library is either build statically OR the user has specified that this library should be installed
if(SORAMA_GLOBAL_INSTALL AND (SORAMA_SHARED OR (SORAMA_GLOBAL_INSTALL_STATIC AND SORAMA_STATIC)))

    # Go through each header inside the list and extract the correct path for the target include dir
    foreach(LIB_HEADER IN LISTS LIB_HEADERS)
        STRING(REPLACE "${CMAKE_CURRENT_LIST_DIR}" "" SORAMA_LIB_PARTIAL_HEADER "${LIB_HEADER}")
        STRING(REGEX REPLACE "/([^/]*)$" "" SORAMA_LIB_SUBDIRECTORY "${SORAMA_LIB_PARTIAL_HEADER}")
        install(FILES ${LIB_HEADER} DESTINATION ${SORAMA_INSTALL_INCLUDEDIR}${SORAMA_LIB_SUBDIRECTORY})
    endforeach()

    install(TARGETS ${LIB_NAME}
            EXPORT "${PROJECT_NAME}Targets"
            LIBRARY DESTINATION ${SORAMA_INSTALL_DIR}
            ARCHIVE DESTINATION ${SORAMA_INSTALL_DIR}
            RUNTIME DESTINATION ${SORAMA_INSTALL_DIR})
        
    # Set install dir of this project as cache variable for reference in install rules that need to add files to the same directory
    set(SORAMA_INSTALL_DIR_${LIB_NAME} ${SORAMA_INSTALL_DIR} CACHE PATH "Set by PyWavelets.cmake" FORCE)
endif()