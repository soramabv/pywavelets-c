# Run shared toolchain files
include(${CMAKE_CURRENT_LIST_DIR}/../dependencies/cmake-modules/toolchains/windows-x64-release-shared.toolchain.cmake)

# These toolchain files run sequentially, code below is run after other toolchains
# To overwrite cached variables use:
# set(VARNAME <value> BOOL/STRING/? "set by windows-x64-release.toolchain.cmake" FORCE)
# To overwrite 'normal' variables use:
# set(VARNAME <value>)
# Only put something here if it is specific for this project, otherwise add it to the shared toolchains